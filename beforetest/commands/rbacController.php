<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\rbac\AuthorRule;


class RbacController extends Controller // יש להקפיד ששם המחלקה יהיה זהה לשם הקובץ
{
    public function actionInit() // יש להריץ פונקציה זו בגית באש
    {
        $auth = Yii::$app->authManager;
        $rule = new AuthorRule; // יש לכתוב בסוף את שם המחלקה. יצירת אובייקט חדש מסוג אוט'ררול
        $auth->add($rule); // הוספת האובייקט
    }
}