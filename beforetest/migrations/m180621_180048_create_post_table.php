<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180621_180048_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
        
            'title' => $this->string(),
            'body' => $this->text(),
            'category' => $this->string(),
            'author' => $this->integer(),
            'status' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
